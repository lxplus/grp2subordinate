RPM_PACKAGE_NAME ?= grp2subordinate
SPECFILE ?= ${RPM_PACKAGE_NAME}.spec
RPM_PACKAGE_VERSION ?= $(shell cat  ${SPECFILE}  | grep '^Version: ' | cut -d " " -f 2)
TARFILE = ${RPM_PACKAGE_NAME}-${RPM_PACKAGE_VERSION}.tar.gz
PYTHON_EXECUTABLES = bin/*

DIST ?= $(shell rpm --eval %{dist})

sources:
	tar cvzf ${TARFILE} --transform="s:^:${RPM_PACKAGE_NAME}-${RPM_PACKAGE_VERSION}/:" bin/* setup.py README.md systemd/*

rpm: sources
	rpmbuild -bb --define 'dist $(DIST)' --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)' $(SPECFILE)
srpm: sources
	rpmbuild -bs --define 'dist $(DIST)' --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)' $(SPECFILE)

