#!/usr/bin/python
"""
  Setup for the grp2subordinate
"""
from distutils.core import setup

setup(name='grp2subordinate',
      version='0.3.1',
      description='grp2subordindate',
      author='Pablo Saiz',
      author_email='pablo.saiz@cern.ch',
      url='i./certmgr/client/certmgr-client.spec',
      scripts=['bin/grp2subordinate',
              ],
      package_data={
          'README.md': ['README.md'],
          }
     )
