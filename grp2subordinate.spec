Name: grp2subordinate
Version: 0.3.1
Release: 1%{?dist}
Summary: Package to generate the files with users

Group: Development/Libraries
License: ASL 2.0
URL: https://gitlab.cern.ch/batch-team/%{name}
Source0: %{name}-%{version}.tar.gz

BuildRequires: python3-devel
BuildRequires: systemd-rpm-macros

Requires: python3,bash,python3-ldap
BuildArch: noarch

%description
This package collects all the users of an egroup, and creates the files
/etc/subuid and /etc/subgid needed by podman.

%prep
%setup -q

%build
%py3_build

%install
%py3_install
rm -rf %{buildroot}/%{python3_sitelib}
install -D -m 0644 systemd/%{name}.service %{buildroot}%{_unitdir}/%{name}.service
install -D -m 0644 systemd/%{name}.timer %{buildroot}%{_unitdir}/%{name}.timer

%files
%doc README.md
%{_bindir}/grp2subordinate
%{_unitdir}/%{name}.service
%{_unitdir}/%{name}.timer

# Use systemd macros in %pre, %post, %preun, %postun sections
%post
%systemd_post %{name}.timer

%preun
%systemd_preun %{name}.timer

%postun
%systemd_postun_with_restart %{name}.timer

%changelog
* Tue Apr 9 2024 Steve Traylen <steve.traylen@cern.ch> - 0.3.1-1
- bugfix - remove extra colon in subuid when using --user option 
  @buzykaev

* Wed Feb 21 2024 Steve Traylen <steve.traylen@cern.ch> - 0.3.0-1
- Improve help instructions in file
- Drop el7 build.

* Fri Sep 29 2023 Steve Traylen <steve.traylen@cern.ch> - 0.2.0-1
- New release

* Mon Jan 24 2022 Steve Traylen <steve.traylen@cern.ch> - 0.1.1-3
- Rebuild for CentOS 9 Stream

* Wed Mar 10 2021 Steve Traylen <steve.traylen@cern.ch> - 0.1.1-2
- Rebuild for CentOS 8 Stream

* Thu Aug 27 2020 Pablo Saiz <pablo.saiz@cern.ch>               - 0.1-1
- Initial version

