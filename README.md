# grp2subordinate

This package maintains the files /etc/subgid and /etc/subuid, which are needed by root-less containers like podman. The file contains a mapping between a user and the subordinate users that the containers can use.

Only the users that appear in a particular egroup (**subordinate-users** by default) will be populated into this file. The service will query the central ldap.cern.ch, getting the list of users and main id. The subordinate ids are calculated by shifting the user id eight bits to the left, and leaving the last 11 bits for the possible subordinate users. The idea has been inspired by [shadow-maint#154](https://github.com/shadow-maint/shadow/issues/154#issuecomment-464119766)

As well as an e-group being specified a list of comma separated users can be specified with the --users option also.
Note that users with UIDs less than 1000 will never be added since their range of subordinates could easily collide with real accounts at CERN.

To protect real high UIDs ever overlapping with low subordinate ranges it is recommended to configure `sssd.conf` with a `max_id = 1999999` to block any real IDs higher ever hight than `1000<<11` which will be the lowest subordinate.

Example:

| *UID*   | *Range Start*            | *Range Size*     | *Range End* |
| --------|--------------------------|------------------|-------------|
| 1000    | 1000<<11 = 2048000       | 1<<11 - 1 = 2047 | 2050047     |
| 1001    | 1001<<11 = 2050048       | 1<<11 - 1 = 2047 | 2052095     |
| 2000000 | 2000000<<11 = 4096000000 | 1<<11 - 1 = 2047 | 4096002047  |

Significantly

```
highest real uid (=2,000,000)
   < lowest subordinate uid (=2,050,047)
   < highest subordinate uid (=4,096,002,047)
   < highest possible uid(=2^32) =4,294,967,295
```
so real and subordinate uids never overlap.


## Unit Configuration
A systemd unit and timer is included in the package that can be tuned to specify the users or e-group names.
e.g. A file can be created `/etc/systemd/system/grp2subordinate.d/settings.service` containing:

```config
[Service]
Environment=GROUP=my_egroup
Environment=USERS=fred,flintstone
```

to provide command line arguments to the script

## Known issues:
* Given that the mapping from user to subordinate UID is documented and reversible, it can be reversed engineer to find out the user who is running the processes
* As users need to be able to see other (especially their own) subordinate process it is impossible to maintain the `/proc` mount option of `hidepid=1`
* In some future universe sssd, glibc and freeipa may all support ability to maintain these ranges centrally. If and when that happens that should be done. [shadow-main#154](https://github.com/shadow-maint/shadow/issues/154) and links from there.

